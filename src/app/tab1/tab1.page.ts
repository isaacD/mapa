import { Component } from '@angular/core';
import * as Leaflet from 'leaflet';
import { antPath } from 'leaflet-ant-path';
import 'leaflet-heatmap.js';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  map: Leaflet.Map;

  propertyList = [];
  constructor() {}

  
  ionViewDidEnter() {
    this.map = Leaflet.map('mapId1').setView([ -2.19616, -79.88621],16);
    Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'Angular LeafLet',
    }).addTo(this.map);
    

   
    fetch('./assets/lugares.json')
      .then(res => res.json())
      .then(lugares => {
        this.propertyList = lugares.lugares;
        this.leafletMap();
      })
      .catch(err => console.error(err));
  }

  leafletMap() {
    for (const property of this.propertyList) {
      Leaflet.marker([property.lat, property.long]).addTo(this.map)
        .bindPopup(property.nombre)
        .openPopup();

        
    }
  }

  ionViewWillLeave() {
    this.map.remove();
  }
}